package com.etnetera.hr.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.etnetera.hr.data.JavaScriptFramework;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFramework, Long> {

    Iterable<JavaScriptFramework> findByName(String name);

    Iterable<JavaScriptFramework> findByHypeLevelBetween(int from, int to);

    Iterable<JavaScriptFramework> findByDeprecationDateBetween(Date from, Date to);

    @Query("select j from JavaScriptFramework j where :version in elements(j.versions)")
    Iterable<JavaScriptFramework> findByVersion(@Param("version") String version);
    
}
