package com.etnetera.hr.data;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 *
 * @author Etnetera
 */
@Entity(name = "JavaScriptFramework")
@Table(name = "JavaScriptFramework")
public class JavaScriptFramework implements Serializable {

    private static final int MAX_NAME_LENGTH = 30;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = MAX_NAME_LENGTH)
    @NotEmpty(message = "NotEmpty")
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @Column(nullable = false)
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date deprecationDate;

    @Column(nullable = false)
    @Min(value = 1)
    @Max(value = 100)
    // hype level in % (1 - 100)
    private int hypeLevel;

    @ElementCollection
    @NotEmpty
    private List<String> versions = new ArrayList<>();

    public JavaScriptFramework() {
    }

    public JavaScriptFramework(String name, ArrayList<String> versions, Date deprecationDate, int hypeLevel) {
        this.name = name;
        this.versions = versions;
        this.deprecationDate = deprecationDate;
        this.hypeLevel = hypeLevel;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getVersions() {
        Collections.sort(versions, Collections.reverseOrder());
        return versions;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    public Date getDeprecationDate() {
        return deprecationDate;
    }

    public void setDeprecationDate(Date deprecationDate) {
        this.deprecationDate = deprecationDate;
    }

    public int getHypeLevel() {
        return hypeLevel;
    }

    public void setHypeLevel(int hypeLevel) {
        this.hypeLevel = hypeLevel;
    }

    public void addVersion(String version) {
        this.versions.add(version);
    }

}