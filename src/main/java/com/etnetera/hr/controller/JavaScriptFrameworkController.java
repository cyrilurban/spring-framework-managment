package com.etnetera.hr.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;

import javax.validation.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController {

    private final JavaScriptFrameworkRepository repository;
    private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private Validator validator = factory.getValidator();
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
        this.repository = repository;
    }

    private Date parseDate(String date) throws MethodArgumentNotValidException {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
        Date parsedData = null;

        try {
            parsedData = sdf.parse(date);
        } catch (ParseException e) {
            BeanPropertyBindingResult result = new BeanPropertyBindingResult(null, "javaScriptFramework");
            result = addError(result, "deprecationDate", "ParseDate");
            throw new MethodArgumentNotValidException(null, result);
        }

        return parsedData;
    }

    private BeanPropertyBindingResult addError(BeanPropertyBindingResult result, String field, String code) {
        FieldError errorField = new FieldError("javaScriptFramework", field, null, false,
                new String[]{code}, null, null);
        result.addError(errorField);
        return result;
    }

    @GetMapping("/frameworks")
    public Iterable<JavaScriptFramework> frameworks() {
        return repository.findAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity add(@RequestBody JavaScriptFramework javaScriptFramework) throws
            MethodArgumentNotValidException {
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(javaScriptFramework, "javaScriptFramework");
        SpringValidatorAdapter adapter = new SpringValidatorAdapter(this.validator);
        adapter.validate(javaScriptFramework, result);

        if (result.hasErrors()) {
            throw new MethodArgumentNotValidException(null, result);
        }

        repository.save(javaScriptFramework);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/add-version", method = RequestMethod.POST)
    public ResponseEntity addVersion(@RequestBody String jsonString) throws MethodArgumentNotValidException,
            IOException {
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(null, "javaScriptFramework");
        JsonNode body = mapper.readTree(jsonString);
        Long id = null;
        String version = null;

        try {
            id = body.get("id").longValue();
        } catch (NullPointerException ex) {
            result = addError(result, "id", "NotNull");
        }

        try {
            version = body.get("version").textValue();
        } catch (NullPointerException ex) {
            result = addError(result, "version", "NotNull");
        }

        if (result.hasErrors()) {
            throw new MethodArgumentNotValidException(null, result);
        }

        try {
            JavaScriptFramework foundFramework = repository.findById(id).get();
            foundFramework.addVersion(version);
            repository.save(foundFramework);
        } catch (NoSuchElementException ex) {
            result = addError(result, "id", "NotExists");
            throw new MethodArgumentNotValidException(null, result);
        }

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestBody JavaScriptFramework javaScriptFramework) throws
            MethodArgumentNotValidException {
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(null, "javaScriptFramework");

        Long id = javaScriptFramework.getId();
        if (id == null) {
            result = addError(result, "id", "NotNull");
            throw new MethodArgumentNotValidException(null, result);
        }

        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            result = addError(result, "id", "NotExists");
            throw new MethodArgumentNotValidException(null, result);
        }

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity update(@RequestBody JavaScriptFramework javaScriptFramework) throws
            MethodArgumentNotValidException {
        BeanPropertyBindingResult result = new BeanPropertyBindingResult(javaScriptFramework, "javaScriptFramework");
        SpringValidatorAdapter adapter = new SpringValidatorAdapter(this.validator);
        adapter.validate(javaScriptFramework, result);

        // check if id is not null
        Long id = javaScriptFramework.getId();
        if (id == null) {
            result = addError(result, "id", "NotNull");
        } else {
            // try existence of editing framework
            try {
                repository.findById(id).get();
            } catch (NoSuchElementException ex) {
                result = addError(result, "id", "NotExists");
            }
        }

        if (result.hasErrors()) {
            throw new MethodArgumentNotValidException(null, result);
        }

        repository.save(javaScriptFramework);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public Iterable<JavaScriptFramework> find(@RequestBody String jsonString) throws MethodArgumentNotValidException,
            IOException {
        JsonNode body = mapper.readTree(jsonString);
        ArrayList<JavaScriptFramework> frameworks = new ArrayList<>();

        try {
            Long id = body.get("id").longValue();
            JavaScriptFramework foundFramework = repository.findById(id).get();
            frameworks.add(foundFramework);
            return frameworks;
        } catch (NullPointerException ex) {
        }

        try {
            String name = body.get("name").textValue();
            return repository.findByName(name);
        } catch (NullPointerException ex) {
        }

        try {
            List<Integer> hypeLevelList = new ArrayList<>();
            if (body.get("hypeLevel").isArray()) {
                for (final JsonNode hypeLevel : body.get("hypeLevel")) {
                    hypeLevelList.add(hypeLevel.asInt());
                }
            }

            if (hypeLevelList.size() == 2) {
                return repository.findByHypeLevelBetween(hypeLevelList.get(0), hypeLevelList.get(1));
            }
        } catch (NullPointerException ex) {
        }

        try {
            List<Date> deprecationDateList = new ArrayList<>();
            if (body.get("deprecationDate").isArray()) {
                for (final JsonNode deprecationDate : body.get("deprecationDate")) {
                    deprecationDateList.add(parseDate(deprecationDate.asText()));
                }
            }

            if (deprecationDateList.size() == 2) {
                return repository.findByDeprecationDateBetween(deprecationDateList.get(0), deprecationDateList.get(1));
            }
        } catch (NullPointerException ex) {
        }

        try {
            String version = body.get("version").textValue();
            return repository.findByVersion(version);
        } catch (NullPointerException ex) {
        }

        return frameworks;
    }

}
