package com.etnetera.hr;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.assertEquals;

import net.minidev.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.JVM)
public class JavaScriptFrameworkTests {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private JavaScriptFrameworkRepository repository;

    private void prepareData() throws Exception {
        ArrayList<String> react_versions = new ArrayList<String>();
        react_versions.add("1.0");
        react_versions.add("1.0.1");

        Date react_date = parseDate("2014-02-11");

        JavaScriptFramework react = new JavaScriptFramework("ReactJS", react_versions, react_date, 80);

        ArrayList<String> vue_versions = new ArrayList<String>();
        vue_versions.add("1");
        vue_versions.add("2");

        Date vue_date = parseDate("2015-02-02");

        JavaScriptFramework vue = new JavaScriptFramework("Vue.js", vue_versions, vue_date, 90);

        repository.save(react);
        repository.save(vue);
    }

    private Long prepareOne() throws Exception {
        ArrayList<String> angular_versions = new ArrayList<String>();
        angular_versions.add("1");
        angular_versions.add("2");

        Date angular_date = parseDate("2018-02-11");

        JavaScriptFramework angular = new JavaScriptFramework("AngularJS", angular_versions, angular_date, 95);

        repository.save(angular);

        return angular.getId();
    }

    private Date parseDate(String date) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
        Date parsedData = null;

        try {
            parsedData = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedData;
    }

    private int getRepositorySize() {
        Iterable<JavaScriptFramework> frameworks = repository.findAll();
        if (frameworks instanceof Collection<?>) {
            return ((Collection<JavaScriptFramework>) frameworks).size();
        }

        return 0;
    }

    @Test
    @Rollback(true)
    public void frameworks() throws Exception {
        mockMvc.perform(get("/frameworks")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)));

        prepareData();

        mockMvc.perform(get("/frameworks")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("ReactJS")))
                .andExpect(jsonPath("$[0].hypeLevel", is(80)))
                .andExpect(jsonPath("$[0].deprecationDate", is("2014-02-11T00:00:00.000+0000")))
                .andExpect(jsonPath("$[0].versions[0]", is("1.0.1")))
                .andExpect(jsonPath("$[0].versions[1]", is("1.0")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Vue.js")))
                .andExpect(jsonPath("$[1].hypeLevel", is(90)))
                .andExpect(jsonPath("$[1].deprecationDate", is("2015-02-02T00:00:00.000+0000")))
                .andExpect(jsonPath("$[1].versions[0]", is("2")))
                .andExpect(jsonPath("$[1].versions[1]", is("1")));
    }

    @Test
    @Rollback(true)
    public void addFramework() throws JsonProcessingException, Exception {
        ArrayList<String> react_versions = new ArrayList<String>();
        react_versions.add("1.0");
        react_versions.add("1.0.1");

        int sizeBefore = getRepositorySize();
        JavaScriptFramework react = new JavaScriptFramework("ReactJS", react_versions, parseDate("2014-02-11"), 80);

        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(react)))
                .andExpect(status().isOk());

        int sizeAfter = getRepositorySize();

        assertEquals(sizeAfter, sizeBefore + 1);
    }

    @Test
    @Rollback(true)
    public void addFrameworkInvalid() throws JsonProcessingException, Exception {
        JavaScriptFramework framework = new JavaScriptFramework();
        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(4)))
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'NotEmpty')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(4)))
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setHypeLevel(-1);
        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(4)))
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setHypeLevel(101);
        mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(4)))
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Max')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());
    }

    @Test
    @Rollback(true)
    public void delete() throws Exception {
        Long frameworkID = prepareOne();

        int sizeBefore = getRepositorySize();

        JavaScriptFramework framework = new JavaScriptFramework();
        framework.setId(frameworkID);
        mockMvc.perform(post("/delete").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isOk());

        int sizeAfter = getRepositorySize();

        assertEquals(sizeAfter, sizeBefore - 1);
    }

    @Test
    @Rollback(true)
    public void deleteInvalid() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework();
        mockMvc.perform(post("/delete").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists());

        // set wrong (not exists) id
        framework.setId(9999L);
        mockMvc.perform(post("/delete").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotExists')]").exists());
    }

    @Test
    @Rollback(true)
    public void addVersion() throws Exception {
        Long frameworkID = prepareOne();

        HashMap<String, Object> body = new HashMap<>();
        body.put("id", frameworkID);
        body.put("version", "3");
        JSONObject json = new JSONObject(body);

        mockMvc.perform(post("/add-version").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());
    }

    @Test
    @Rollback(true)
    public void addVersionInvalid() throws Exception {
        Long frameworkID = prepareOne();
        HashMap<String, Object> body = new HashMap<>();

        // none ID
        body.put("version", "2.0.1");
        JSONObject json = new JSONObject(body);
        mockMvc.perform(post("/add-version").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists());

        // none version
        body = new HashMap<>();
        body.put("id", frameworkID);
        json = new JSONObject(body);
        mockMvc.perform(post("/add-version").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'version')][?(@.message == 'NotNull')]").exists());

        // none ID and version
        body = new HashMap<>();
        json = new JSONObject(body);
        mockMvc.perform(post("/add-version").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(2)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'version')][?(@.message == 'NotNull')]").exists());

        // set wrong (not exists) id
        body = new HashMap<>();
        body.put("version", "2.0.1");
        body.put("id", 9999L);
        json = new JSONObject(body);
        mockMvc.perform(post("/add-version").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotExists')]").exists());
    }

    @Test
    @Rollback(true)
    public void updateFramework() throws JsonProcessingException, Exception {
        Long frameworkId = prepareOne();

        ArrayList<String> react_versions = new ArrayList<String>();
        react_versions.add("1.0");
        JavaScriptFramework react = new JavaScriptFramework("ReactJS", react_versions, parseDate("2014-02-11"), 80);
        react.setId(frameworkId);

        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(react)))
                .andExpect(status().isOk());

        JavaScriptFramework foundFramework = repository.findById(frameworkId).get();
        assertEquals(frameworkId, foundFramework.getId());
        assertEquals("ReactJS", foundFramework.getName());
        assertEquals(80, foundFramework.getHypeLevel());
    }

    @Test
    @Rollback(true)
    public void updateInvalid() throws Exception {
        JavaScriptFramework framework = new JavaScriptFramework();
        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(5)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'NotEmpty')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(5)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setHypeLevel(-1);
        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(5)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Min')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setHypeLevel(101);
        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(5)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Max')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());

        framework.setId(999L);
        mockMvc.perform(post("/update").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(framework)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(5)))
                .andExpect(jsonPath("$.errors[?(@.field == 'id')][?(@.message == 'NotExists')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'name')][?(@.message == 'Size')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'NotNull')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'hypeLevel')][?(@.message == 'Max')]").exists())
                .andExpect(jsonPath("$.errors[?(@.field == 'versions')][?(@.message == 'NotEmpty')]").exists());
    }

    @Test
    @Rollback(true)
    public void find() throws Exception {
        HashMap<String, Object> body = new HashMap<>();

        // prepare framework
        ArrayList<String> angular_versions = new ArrayList<String>();
        angular_versions.add("1");
        Date angular_date = parseDate("2018-02-11");
        JavaScriptFramework angular = new JavaScriptFramework("AngularJS", angular_versions, angular_date, 95);
        repository.save(angular);

        // by id
        body.put("id", angular.getId());
        JSONObject json = new JSONObject(body);
        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());

        // by name
        body = new HashMap<>();
        body.put("name", angular.getName());
        json = new JSONObject(body);
        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());

        // by deprecationDate
        body = new HashMap<>();
        body.put("deprecationDate", new String[]{"2017-01-01", "2019-01-01"});
        json = new JSONObject(body);
        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());

        // by hypeLevel
        body = new HashMap<>();
        body.put("hypeLevel", new int[]{10, 95});
        json = new JSONObject(body);
        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());

        // by versions
        body = new HashMap<>();
        body.put("version", "1");
        json = new JSONObject(body);
        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isOk());

        // here should be more complex tests...
    }

    @Test
    @Rollback(true)
    public void findInvalid() throws Exception {
        HashMap<String, Object> body = new HashMap<>();

        // bad formatted date
        body.put("deprecationDate", new String[]{"2018-01-01", "2018-01"});
        JSONObject json = new JSONObject(body);

        mockMvc.perform(post("/find").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(json)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[?(@.field == 'deprecationDate')][?(@.message == 'ParseDate')]")
                        .exists());
    }

}
