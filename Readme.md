# JavaScript Framework Manager
Simple app for managing JavaScript frameworks implemented in **Spring**.

### How to run
run via **gradle** with task **bootRun**

### REST API fast overview
* List of frameworks:
    * GET /frameworks
* Add new framework: name, deprecationDate(yyyy-MM-dd), hypeLevel(1, 100), versions[version, version,...]
    * POST /add
* Update framework: id, name, deprecationDate(yyyy-MM-dd), hypeLevel(1, 100), versions[version, version,...]
    * POST /update
* Add new version of framework: version
    * POST /add-version
* Delete framework: id
    * POST /delete
* Find framework: One field from: id, name, deprecationDate(from, to), hypeLevel(from, to), version
    * POST /find

Request and Respons media type is **application/json**

Common response codes indicating (1) success, (2) failure due to client-side problem, (3) failure due to server-side problem:
* 200 - OK
* 400 - Bad Request
* 500 - Internal Server Error
